# Guide d’ouverture des algorithmes et codes sources du pôle ministériel

## Objectifs de la démarche de publication

La loi du 7 octobre 2016 dite « Loi Lemaire » prévoit l’**ouverture par défaut** de l’ensemble des données publiques d’intérêt économique, social, sanitaire ou environnemental.

La circulaire n°6264/SG du premier ministre datée du 27 avril 2021 étend cette **ouverture aux algorithmes et codes sources publics**.

Dans un premier temps, le pôle ministériel a décidé de **se focaliser** sur l’ouverture des algorithmes et codes sources (ACS) qui présentent un **intérêt général** en termes de **connaissance** et de **transparence** de l’action publique.

## Objectifs du guide

Afin de **convaincre les responsables décideurs** de l’intérêt et de l’importance de publier et ainsi faire progresser significativement le taux de publication des ACS au sein du pôle ministériel, le guide **présente les gains** générés par la démarche de publication, notamment en matière de **réutilisation, d’amélioration des codes, de renforcement de leur sécurité**.

Il propose également un **accompagnement pratique et concret** sur la **procédure de publication**. Son objectif principal est de **répondre à l’ensemble des interrogations** (processus, licences, animation, etc.) **que se posent les services** lors du processus de publication de leurs ACS.

## Licence

Ce guide est mis à disposition sous licence Creative Commons CC BY-SA 4.0 Attribution - Partage dans les mêmes conditions. Voir https://creativecommons.org/licenses/by-sa/4.0/legalcode.fr


Accéder à la [**version numérique**](guide_ouverture_algorithmes_codes_sources_mtect.pdf) du guide

Accéder à la [**version imprimable**](Guide_ouverture_algorithmes_codes_sources_mtect-PRINT.pdf) du guide
